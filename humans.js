import config from './config.js'
export default class Humans {
    constructor(x, y) {
        this.width = 0.05
        this.height = 0.08
        this.x = x
        this.y = y
        this.reRender = true
    }

    render(canvas, ctx) {
        if(this.reRender) {
            ctx.beginPath();
            ctx.lineWidth = 1.5
            ctx.rect(this.x, this.y, this.width*canvas.width, this.height*canvas.height);
            ctx.stroke();
        }
    }

    collide(caven, canvas) {
        
        let recWidth = (this.width * canvas.width) /2
        let recHeight = (this.height * canvas.height) / 2
        let centerX = (this.x + recWidth)
        let centerY = (this.y + recHeight)

        let cavenX = (caven.x * canvas.width)
        let cavenY = (caven.y * canvas.height)
        let dx = Math.abs(cavenX - centerX)
        let dy = Math.abs(cavenY - centerY)

        let radius = caven.radius * canvas.height

        if(dx > (radius + recWidth) || dy > (radius + recHeight)) return false

        let cavenDistanceX = Math.abs(cavenX - this.x - recWidth)
        let cavenDistanceY = Math.abs(cavenY - this.y - recHeight)

        if(cavenDistanceX <= recWidth) return true

        if(cavenDistanceY <= recHeight) return true

        let cornerThingy = Math.pow(cavenDistanceX - recWidth, 2) + Math.pow(cavenDistanceY - recHeight, 2)

        return (cornerThingy <= (Math.pow(radius, 2)))

        
    }

    canTunnelBeteen(caven, value) {

    }
}
