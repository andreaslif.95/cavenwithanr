import World from './world.js'
import Caven from './caven.js'
import config from './config.js'


const world = new World(document)
world.create()
const phases = [phaseOne, phaseTwo, phaseThree, () =>  false]
let currentPhase = 0
const cavens = Math.random()*config.world.misc.maxCavens

setInterval(() => {
    if(!phases[currentPhase]()) currentPhase++
    world.render()
}, 20)



function phaseOne() {
    if(world.cavens.length > cavens) {
        return false
    }
    world.addCaven(new Caven())
    return true
}

function phaseTwo() {
    world.addGoldLine()
    return false
}
function phaseThree() {
    world.generateHuman()
    return false
}