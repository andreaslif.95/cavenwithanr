import config from './config.js'
export default class GoldLine {
    constructor(pos, pos2) {
        this.pos = pos
        this.pos2 = pos2
        this.reRender = true

    }

    render(canvas, ctx) {
        if(this.reRender) {
            const width = canvas.width
            const height = canvas.height
            ctx.beginPath()
            ctx.moveTo(this.pos.x, this.pos.y)
            ctx.lineTo(this.pos2.x, this.pos2.y)
            ctx.strokeStyle = config.goldLine.fill
            ctx.lineWidth = config.goldLine.lineWidth * width
            ctx.stroke()
            ctx.closePath()
            this.reRender = false
        }
        
    }
}
