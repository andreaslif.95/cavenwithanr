import config from './config.js'
import Tunnel from './tunnel.js'
import GoldLine from './goldline.js'
import Humans from './humans.js'
export default class World {
    constructor() {
        this.canvas = document.createElement('canvas')
        this.cavens = []
        this.entities = []
        this.tunnels = []
        this.humans = []
        this.goldLine = null
    }

    create () {
        this.canvas.height = window.innerHeight
        this.canvas.width = window.innerWidth
        Object.assign(this.canvas.style, config.world.style)
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.firstChild)

        //Creates floor line.
        this.context.lineWidth = 0.5
        this.context.moveTo(0, this.canvas.height*0.1)
        this.context.lineTo(this.canvas.width, this.canvas.height*0.1)
        this.context.stroke()

    }

    addEntity(entity) {
        this.entities.push(entity)
    }

    addCaven(caven) {
        for(let i = 0; i < this.cavens.length; i++) {
            if(caven.collide(this.cavens[i])) {
                return false
            }
        }
        for(let i = 0; i < this.cavens.length; i++) {
            if(caven.canTunnelBeteen(this.cavens[i], config.world.misc.lengthToTunnel)){
               this.tunnels.push(new Tunnel(caven, this.cavens[i]))
            }
        }
        this.cavens.push(caven)
        return true
    }

    addGoldLine() {

        const pos = {x: 0, y: Math.random()*this.canvas.height }
        const pos2 = {x: this.canvas.width, y: Math.random()*this.canvas.height }
        if(pos.y < (this.canvas.height*0.1)) {
            pos.y = this.canvas.height*0.1
        }
        if(pos2.y < (this.canvas.height*0.1)) {
            pos2.y = this.canvas.height*0.1
        }
        this.goldLine = new GoldLine(pos, pos2)
        this.goldLine.render(this.canvas, this.context)
    }


    render() {
        this.cavens.forEach(caven => {
            caven.preRender(this.canvas, this.context)
        })

        this.cavens.forEach(caven => {
            caven.render(this.canvas, this.context)
        })
        this.tunnels.forEach(tunnel => {
            tunnel.render(this.canvas, this.context)
        })
        if(this.goldLine !== null) {
            this.goldLine.render(this.canvas, this.context)
        }
    }

    generateHuman() {
        const one = this.canvas.width/10

        const human = new Humans(Math.random()*10 * one, this.canvas.height*0.09)
        this.cavens.forEach(caven => {
            console.log(human.collide(caven, this.canvas))
        } )
        human.render(this.canvas, this.context)
        this.humans.push(human)


    }

}
